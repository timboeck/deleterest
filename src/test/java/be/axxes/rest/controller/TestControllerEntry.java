package be.axxes.rest.controller;

import org.junit.Test;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestControllerEntry extends BaseControllerIT {

    @Test
    public void testDelete() throws Exception {
        performGetJson("/entries/1")
                .andExpect(status().isOk())
        ;
        performDelete("/entries/1")
                .andExpect(status().isNoContent())
        ;
        performGetJson("/entries/1")
                .andExpect(status().isNotFound());
    }

}
