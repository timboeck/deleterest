package be.axxes.rest.controller;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml", "classpath:spring/dispatcher-servlet.xml"})
@WebAppConfiguration
public abstract class BaseControllerIT {

    protected MockMvc mockMvc;

    @Resource
    private WebApplicationContext webApplicationContext;

    @Before
    public void baseSetUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();

    }

    protected ResultActions performGetJson(final String url) throws Exception {
        MockHttpServletRequestBuilder request = get(url);
        return mockMvc.perform(request);
    }

    protected ResultActions performPost(final String url, final String body) throws Exception {
        MockHttpServletRequestBuilder request = createPostRequest(url, body);
        return mockMvc.perform(request);
    }

    protected ResultActions performDelete(final String url) throws Exception {
        MockHttpServletRequestBuilder request = delete(url);
        return mockMvc.perform(request);
    }

    private MockHttpServletRequestBuilder createPostRequest(final String url, final String body) {
        MockHttpServletRequestBuilder request = post(url);
        return addJsonBody(request, body);
    }

    private MockHttpServletRequestBuilder addJsonBody(final MockHttpServletRequestBuilder request, final String body) {
        if (body != null) {
            request.content(body);
        }
        request.contentType(MediaType.APPLICATION_JSON);
        return request;
    }
}
