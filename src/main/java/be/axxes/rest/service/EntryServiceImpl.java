package be.axxes.rest.service;

import be.axxes.rest.model.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class EntryServiceImpl implements EntryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryServiceImpl.class);

    private Map<String, Entry> entries = new HashMap<String, Entry>();

    @PostConstruct
    public void init() {
        putEntry("1");
        putEntry("2");
        putEntry("3");
        putEntry("4");
        putEntry("5");
    }

    private void putEntry(final String id) {
        Entry entry = new Entry(id, "value of " + id);
        entries.put(entry.getId(), entry);
    }

    @Override
    public Entry getEntry(final String id) {
        LOGGER.debug("retrieving value {}", id);
        return entries.get(id);
    }

    @Override
    public void deleteEntry(final String id) {
        //TODO
    }
}
