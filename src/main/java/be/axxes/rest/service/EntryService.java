package be.axxes.rest.service;

import be.axxes.rest.model.Entry;

public interface EntryService {

    Entry getEntry(final String id);

    void deleteEntry(String id);
}
