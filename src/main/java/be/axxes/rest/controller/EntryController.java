package be.axxes.rest.controller;

import be.axxes.rest.controller.dto.EntryDto;
import be.axxes.rest.model.Entry;
import be.axxes.rest.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/entries")
public class EntryController {

    @Autowired
    private EntryService entryService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EntryDto> getById(final @PathVariable String id) {
        Entry entry = entryService.getEntry(id);
        if (entry == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(new EntryDto(entry), HttpStatus.OK);
        }
    }

    //TODO
    public ResponseEntity<EntryDto> delete(
            //TODO
    ) {
        entryService.deleteEntry(null);
        //TODO
        return null;
    }
}
