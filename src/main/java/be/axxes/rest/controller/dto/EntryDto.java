package be.axxes.rest.controller.dto;

import be.axxes.rest.model.Entry;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EntryDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("value")
    private String value;

    public EntryDto(final Entry entry) {
        id = entry.getId();
        value = entry.getValue();
    }

}
